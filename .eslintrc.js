module.exports = {
  parser: 'babel-eslint',
  extends: ['plugin:react/recommended', 'prettier'],
  env: {
    browser: true,
  },
}
