export default (theme) => ({
  position: 'sticky',
  bottom: -theme.spacing(1),
  paddingBottom: theme.spacing(1),
})
