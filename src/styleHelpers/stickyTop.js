export default (theme) => ({
  position: 'sticky',
  // AppBar height minus main padding
  top: theme.spacing(7) - theme.spacing(1),
  '@media (min-width:0px) and (orientation: landscape)': {
    top: theme.spacing(6) - theme.spacing(1),
  },
  '@media (min-width:600px)': {
    top: theme.spacing(8) - theme.spacing(1),
  },
})
