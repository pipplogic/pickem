import { stickyTop, stickyBottom } from '../styleHelpers'

export default (theme) => {
  const headerColor = theme.palette.grey[300]
  return {
    header: {
      color: theme.palette.getContrastText(headerColor),
      backgroundColor: headerColor,
      ...stickyTop(theme),
    },
    alert: {
      marginBottom: theme.spacing(3),
    },
    actions: {
      display: 'flex',
      justifyContent: 'center',
      backgroundColor: theme.palette.background.default,
      ...stickyBottom(theme),
      paddingTop: theme.spacing(1),
    },
    load: {
      animation: '$spin 2s linear infinite',
    },
    '@keyframes spin': {
      from: { transform: 'rotate(0deg)' },
      to: { transform: 'rotate(360deg)' },
    },
    rightIcon: {
      marginLeft: theme.spacing(1),
    },
  }
}
