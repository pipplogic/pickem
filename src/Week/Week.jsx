import Button from '@material-ui/core/Button'
import Divider from '@material-ui/core/Divider'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListSubheader from '@material-ui/core/ListSubheader'
import Alert from '@material-ui/lab/Alert'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Autorenew from '@material-ui/icons/Autorenew'
import Check from '@material-ui/icons/Check'
import Error from '@material-ui/icons/Error'
import Save from '@material-ui/icons/Save'
import cx from 'classnames'
import PropTypes from 'prop-types'
import React, { Fragment, useEffect } from 'react'
import StatusPage from '../StatusPage'
import Game from '../Game'
import GameHeader from '../GameHeader'
import { idType } from '../propType'
import styles from './styles'

const useStyles = makeStyles(styles)

export default function Week({
  className,
  error,
  loadEmAll,
  loading,
  gameIds,
  savePicks,
  poolId,
  weekId,
  userId,
  saving,
  saveError,
  modified,
}) {
  const classes = useStyles()

  useEffect(() => {
    loadEmAll({ poolId, userId, weekId })
  }, [loadEmAll, poolId, userId, weekId])

  if (loading) {
    return <StatusPage className={className} status="loading" />
  }

  if (error) {
    return <StatusPage className={className} status="error" />
  }
  if (!poolId) {
    return (
      <StatusPage className={className} status="error">
        You must join a pool
      </StatusPage>
    )
  }

  return (
    <div className={className}>
      <List disablePadding>
        <ListSubheader classes={{ root: classes.header }}>
          <GameHeader />
        </ListSubheader>
        {gameIds &&
          gameIds.map((gameId) => (
            <React.Fragment key={gameId}>
              <ListItem>
                <Game gameId={gameId} poolId={poolId} userId={userId} />
              </ListItem>
              <Divider />
            </React.Fragment>
          ))}
      </List>
      <div className={classes.actions}>
        <Button
          variant="contained"
          color="secondary"
          disabled={!modified || userId != null}
          onClick={savePicks({ poolId, weekId })}
        >
          Save
          {saving && (
            <Autorenew className={cx(classes.rightIcon, classes.load)} />
          )}
          {!saving && (
            <Fragment>
              {saveError && (
                <Error className={cx(classes.rightIcon, classes.error)} />
              )}
              {!saveError && (
                <Fragment>
                  {modified && <Save className={classes.rightIcon} />}
                  {!modified && <Check className={classes.rightIcon} />}
                </Fragment>
              )}
            </Fragment>
          )}
        </Button>
      </div>
    </div>
  )
}

Week.propTypes = {
  weekId: idType.isRequired,
  className: PropTypes.string,
  error: PropTypes.bool,
  loading: PropTypes.bool,
  gameIds: PropTypes.arrayOf(idType.isRequired).isRequired,
  poolId: idType.isRequired,
  userId: idType,
  savePicks: PropTypes.func.isRequired,
  saving: PropTypes.bool.isRequired,
  saveError: PropTypes.shape({ details: PropTypes.string }),
  modified: PropTypes.bool.isRequired,
  loadEmAll: PropTypes.func.isRequired,
}

Week.defaultProps = {
  className: null,
  error: false,
  loading: false,
  saveError: null,
}
