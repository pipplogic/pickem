import { combineReducers } from 'redux'

import { loadWeek as apiLoadWeek } from '../api'
import extendsReducer from '../reducers/extendsReducer'

const LOADING = 'pickem/week/loading'
const LOADED = 'pickem/week/loaded'
const LOAD_ERROR = 'pickem/week/loadError'
const TEAMS = 'pickem/week/teams'
const GAMES = 'pickem/week/games'
const WEEK = 'pickem/week/week'
const PICKS = 'pickem/week/picks'

const loadingReducer = (state = false, action = {}) => {
  switch (action.type) {
    case LOADING:
      return true
    case LOADED:
    case LOAD_ERROR:
      return false
    default:
      return state
  }
}

const errorReducer = (state = false, action = {}) => {
  switch (action.type) {
    case LOADING:
    case LOADED:
      return false
    case LOAD_ERROR:
      return true
    default:
      return state
  }
}

export default combineReducers({
  error: errorReducer,
  games: extendsReducer(GAMES),
  loading: loadingReducer,
  teams: extendsReducer(TEAMS),
  weeks: extendsReducer(WEEK),
  picks: extendsReducer(PICKS),
})

export const getGameById = (state) => (gameId) => state.games[gameId]

export const getGameIdsForWeek = (state) => (weekId) => state.weeks[weekId]

export const getWeekGames = (state) => (gameId) => {
  const weekId = getGameById(state)(gameId)
  return getGameIdsForWeek(state)(weekId)
}

export const getTeamById = (state) => (teamId) => state.teams[teamId]

const getTeamsById = ({ teams }) =>
  teams.reduce(
    (teamsById, team) => ({
      ...teamsById,
      [team.teamId]: team,
    }),
    {},
  )

const getGamesById = ({ games }) =>
  games.reduce(
    (gamesById, game) => ({
      ...gamesById,
      [game.gameId]: game,
    }),
    {},
  )

const getGameIds = ({ games }) => games.map(({ gameId }) => gameId)

export const loadWeek = (weekId) => (dispatch) => {
  dispatch({ type: LOADING })
  return apiLoadWeek(weekId)
    .then((weekInfo) => {
      dispatch({ type: GAMES, payload: getGamesById(weekInfo) })
      dispatch({ type: TEAMS, payload: getTeamsById(weekInfo) })
      dispatch({ type: WEEK, payload: { [weekId]: getGameIds(weekInfo) } })
      dispatch({ type: LOADED })
      return weekInfo
    })
    .catch((error) => {
      dispatch({ type: LOAD_ERROR, error })
    })
}
