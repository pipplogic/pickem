/* eslint-env jest  */

import getInitialPicks from './initialPicks'

const poolId = 'poolId'
const now = new Date().getTime()
const past = now - 1e7
const future = now + 1e7

const fullPicks = [
  {
    gameId: 1,
    poolId,
    score: 1,
    teamId: 1,
  },
  {
    gameId: 2,
    poolId,
    score: 2,
    teamId: 2,
  },
  {
    gameId: 3,
    poolId,
    score: 3,
    teamId: 3,
  },
]

const games = [
  {
    gameId: 1,
    gameTime: past,
  },
  {
    gameId: 2,
    gameTime: future,
  },
  {
    gameId: 3,
    gameTime: future,
  },
]

const scoreOptions = [3, 2, 1]

describe('initialPicks', () => {
  it('should use initial picks if fully filled out for week', () => {
    const initialPicks = getInitialPicks({
      knownPicks: fullPicks,
      games,
      scoreOptions,
      poolId,
    })

    expect(initialPicks.length).toEqual(fullPicks.length)

    fullPicks.forEach((fullPick) => {
      const firstGame = initialPicks.filter(
        (game) => game.gameId === fullPick.gameId,
      )[0]
      expect(firstGame).toEqual(fullPick)
    })
  })

  it('should default when there are no picks made', () => {
    const initialPicks = getInitialPicks({
      knownPicks: [],
      games,
      scoreOptions,
      poolId,
    })

    // Should skip first game since it was in the past
    const firstPick = initialPicks.filter(
      (game) => game.gameId === games[0].gameId,
    )[0]
    expect(firstPick.score).toBeUndefined()
    expect(firstPick.teamId).toBe(undefined)

    const secondPick = initialPicks.filter(
      (game) => game.gameId === games[1].gameId,
    )[0]
    expect(secondPick.score).toBe(3)
    expect(secondPick.teamId).toBe(undefined)

    const thirdPick = initialPicks.filter(
      (game) => game.gameId === games[2].gameId,
    )[0]
    expect(thirdPick.score).toBe(2)
    expect(thirdPick.teamId).toBe(undefined)
  })

  it('should use partial data when it exists', () => {
    const initialPicks = getInitialPicks({
      knownPicks: [{ gameId: 1, score: 2, teamId: 1 }],
      games,
      scoreOptions,
      poolId,
    })

    // Should skip first game since it was in the past
    const firstPick = initialPicks.filter(
      (game) => game.gameId === games[0].gameId,
    )[0]
    expect(firstPick.score).toBe(2)
    expect(firstPick.teamId).toBe(1)

    const secondPick = initialPicks.filter(
      (game) => game.gameId === games[1].gameId,
    )[0]
    expect(secondPick.score).toBe(3)
    expect(secondPick.teamId).toBe(undefined)

    const thirdPick = initialPicks.filter(
      (game) => game.gameId === games[2].gameId,
    )[0]
    expect(thirdPick.score).toBe(1)
    expect(thirdPick.teamId).toBe(undefined)
  })

  it('should use partial data when it exists', () => {
    const initialPicks = getInitialPicks({
      knownPicks: [{ gameId: 3, score: 3, teamId: 3 }],
      games,
      scoreOptions,
      poolId,
    })

    // Should skip first game since it was in the past
    const firstPick = initialPicks.filter(
      (game) => game.gameId === games[0].gameId,
    )[0]
    expect(firstPick.score).toBe(undefined)
    expect(firstPick.teamId).toBe(undefined)

    const secondPick = initialPicks.filter(
      (game) => game.gameId === games[1].gameId,
    )[0]
    expect(secondPick.score).toBe(2)
    expect(secondPick.teamId).toBe(undefined)

    const thirdPick = initialPicks.filter(
      (game) => game.gameId === games[2].gameId,
    )[0]
    expect(thirdPick.score).toBe(3)
    expect(thirdPick.teamId).toBe(3)
  })
})
