const isGamePast = (now = new Date().getTime()) => (game) =>
  game.gameTime <= now

export default function getInitialPicks({
  knownPicks,
  games,
  poolId,
  userId,
  scoreOptions,
  now = new Date().getTime(),
}) {
  const pickedPoints = knownPicks.map((pick) => pick.score).filter(Boolean)

  const unpickedPoints = scoreOptions.filter(
    (option) => !pickedPoints.includes(option),
  )

  const pickedGames = knownPicks.map((game) => game.gameId)

  const unpickedGames = games.filter(
    (game) => !pickedGames.includes(game.gameId),
  )

  const unpickedGamesPast = unpickedGames
    .filter((game) => isGamePast(now)(game))
    .map((game) => game.gameId)
    .map((gameId) => ({
      gameId,
      poolId,
      userId,
    }))

  const unpickedGamesRemaining = unpickedGames
    .filter((game) => !isGamePast(now)(game))
    .map((game) => game.gameId)
    .map((gameId, index) => ({
      gameId,
      poolId,
      userId,
      score: unpickedPoints[index],
    }))

  return [
    ...knownPicks.map((pick) => ({ ...pick, userId })),
    ...unpickedGamesPast,
    ...unpickedGamesRemaining,
  ]
}
