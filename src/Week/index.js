import { connect } from 'react-redux'
import Week from './Week'
import { loadWeek } from './weekDuck'
import {
  savingPicks,
  savedPicks,
  errorSavingPicks,
} from '../reducers/picksDuck'
import { getWeekState } from '../reducers'
import {
  getGameIdsForWeek,
  isSavingPicks,
  picksError,
  getPick,
  arePicksModified,
} from '../reducers/selectors'

import { addPicks, addSnack, setScoreOptions } from '../reducers/actionCreators'

import {
  savePicks as apiSavePicks,
  loadPicks as apiLoadPicks,
  loadPoolOptions as apiLoadPoolOptions,
} from '../api'
import getInitialPicks from './initialPicks'

const mapStateToProps = (state, { weekId }) => {
  const weekState = getWeekState(state)

  const { loading, error } = weekState
  const gameIds = getGameIdsForWeek(state)(weekId) || []
  const saving = isSavingPicks(state)
  const saveError = picksError(state)
  const modified = arePicksModified(state)

  return {
    loading,
    error,
    gameIds,
    saving,
    saveError,
    modified,
  }
}

const savePicks = ({ poolId, weekId }) => (dispatch, getState) => () => {
  const state = getState()
  const gameIds = getGameIdsForWeek(state)(weekId)

  const getGamePick = getPick(state)(poolId)

  const picks = gameIds.map(getGamePick)
  dispatch(savingPicks())
  apiSavePicks({ poolId, picks })
    .then(() => {
      dispatch(savedPicks())
      dispatch(addSnack({ message: 'Saved!' }))
    })
    .catch((err) => {
      dispatch(errorSavingPicks(err))
      dispatch(addSnack({ message: 'Error!', actionMessage: 'Details' }))
    })
}

const loadEmAll = ({ weekId, poolId, userId }) => (dispatch) => {
  const weekinfoAsync = loadWeek(weekId)(dispatch)
  if (!poolId || !weekId) {
    return
  }
  const loadedPicksAsync = apiLoadPicks({ poolId, weekId, userId })
  // TODO eventually use these instead of the global options
  const scoreOptionsAsync = apiLoadPoolOptions({ poolId, weekId })

  Promise.all([loadedPicksAsync, weekinfoAsync, scoreOptionsAsync]).then(
    ([loadedPicks, weekInfo, scoreOptions]) => {
      const defaultedPicks = getInitialPicks({
        knownPicks: loadedPicks,
        games: (weekInfo || {}).games || [],
        poolId,
        userId,
        scoreOptions,
      })

      dispatch(setScoreOptions({ poolId, weekId, options: scoreOptions }))

      dispatch(addPicks(defaultedPicks))
    },
  )
}

export default connect(
  mapStateToProps,
  {
    loadEmAll,
    savePicks,
  },
)(Week)
