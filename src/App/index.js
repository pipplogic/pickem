import { connect } from 'react-redux'

import App from './App'
import { mapDispatch } from './reduxMappers'

export default connect(
  null,
  mapDispatch,
)(App)
