import Paper from '@material-ui/core/Paper'
import makeStyles from '@material-ui/core/styles/makeStyles'

import PropTypes from 'prop-types'
import React, { useEffect } from 'react'
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom'

import Login from '../Login'
import Register from '../Register'
import Footer from '../Footer'
import ForgotPassword from '../ForgotPassword'
import Header from '../Header'
import ConfirmUser from '../ConfirmUser'
import Theme from '../Theme'
import RequireLogin from '../RequireLogin'
import LoggedInView from '../LoggedInView'
import Snackbar from '../Snackbar'
import styles from './styles'

const useStyles = makeStyles(styles)

export default function App({ loadExistingSession }) {
  const classes = useStyles(styles)
  useEffect(loadExistingSession, [])

  return (
    <Theme>
      <RequireLogin>
        <BrowserRouter>
          <LoggedInView />
        </BrowserRouter>
      </RequireLogin>
      <RequireLogin inverted>
        <div className={classes.root}>
          <Header />
          <Paper
            component="main"
            elevation={2}
            classes={{ root: classes.body }}
          >
            <BrowserRouter>
              <Switch>
                <Route exact path="/register" component={Register} />
                <Route
                  exact
                  path="/forgot-password"
                  component={ForgotPassword}
                />
                <Route exact path="/confirm" component={ConfirmUser} />
                <Route exact path="/login" component={Login} />
                <Redirect to="/login" />
              </Switch>
            </BrowserRouter>
          </Paper>
          <Footer />
        </div>
      </RequireLogin>

      <Snackbar />
    </Theme>
  )
}

App.propTypes = {
  loadExistingSession: PropTypes.func.isRequired,
}
