import qs from 'qs'

export default function modifyParams(search, modifiedParams) {
  const existingObject = qs.parse(search, { ignoreQueryPrefix: true })
  const newParams = { ...existingObject, ...modifiedParams }
  const result = qs.stringify(newParams)
  return result
}
