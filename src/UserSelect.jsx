import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import { PropTypes } from 'prop-types'
import modifyParams from './routerHelpers/modifyParams'

import { idType } from './propType'

import Dropdown from './Dropdown'
import { getDisplayName } from './reducers/selectors'

function UserSelect({ userId, users, location }) {
  const options = users.map((user) => ({
    value: user.id,
    label: getDisplayName(user),
  }))
  return (
    <Dropdown
      value={userId}
      options={options}
      menuItemProps={(option) => ({
        component: Link,
        to: {
          search: modifyParams(location.search, {
            userId: option.value,
          }),
        },
      })}
    />
  )
}

UserSelect.propTypes = {
  userId: idType.isRequired,
  users: PropTypes.arrayOf(
    PropTypes.shape({
      id: idType.isRequired,
      firstName: PropTypes.string.isRequired,
      lastName: PropTypes.string.isRequired,
    }).isRequired,
  ).isRequired,
  location: PropTypes.shape({ search: PropTypes.string }).isRequired,
}

export default withRouter(UserSelect)
