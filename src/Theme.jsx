import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import CssBaseline from '@material-ui/core/CssBaseline'
import PropTypes from 'prop-types'
import React from 'react'
import teal from '@material-ui/core/colors/teal'
import red from '@material-ui/core/colors/red'

const theme = createMuiTheme({
  palette: {
    secondary: teal,
    status: {
      success: teal,
      error: red,
    },
  },
  props: {
    MuiTypography: {
      /* color: 'inherit', */
    },
    MuiTableCell: {
      size: 'small',
    },
  },
})

function Theme({ children }) {
  return (
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </MuiThemeProvider>
  )
}

Theme.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Theme
