import { getSnack } from '../reducers/selectors'
import { snackClose, snackExit } from '../reducers/actionCreators'

export const mapState = (state) => {
  const { message, key, isOpen, actionMessage } = getSnack(state)

  return { message, key, isOpen, actionMessage }
}

export const mapDispatch = {
  handleClose: snackClose,
  handleExit: snackExit,
}
