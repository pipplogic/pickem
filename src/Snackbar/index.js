import { connect } from 'react-redux'

import { mapState, mapDispatch } from './reduxMappers'
import Snackbar from './Snackbar'

export default connect(
  mapState,
  mapDispatch,
)(Snackbar)
