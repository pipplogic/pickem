import React, { Fragment } from 'react'

import IconButton from '@material-ui/core/IconButton'
import Snackbar from '@material-ui/core/Snackbar'
import CloseIcon from '@material-ui/icons/Close'
import Button from '@material-ui/core/Button'

import PropTypes from 'prop-types'

export default function MySnackbar({
  message,
  isOpen,
  actionMessage,
  handleClose,
  handleExit,
}) {
  return (
    <Snackbar
      open={isOpen}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
      autoHideDuration={6000}
      ContentProps={{ 'aria-describedby': 'snack-message' }}
      message={<span id="snack-message">{message}</span>}
      onClose={handleClose}
      onExit={handleExit}
      action={
        <Fragment>
          {actionMessage && (
            <Button
              color="inherit"
              size="small"
              onClick={() => {
                window.alert('TODO')
              }}
            >
              {actionMessage}
            </Button>
          )}
          <IconButton aria-label="Close" color="inherit" onClick={handleClose}>
            <CloseIcon />
          </IconButton>
        </Fragment>
      }
    />
  )
}
MySnackbar.propTypes = {
  message: PropTypes.string,
  isOpen: PropTypes.bool.isRequired,
  actionMessage: PropTypes.string,
  handleClose: PropTypes.func.isRequired,
  handleExit: PropTypes.func.isRequired,
}

MySnackbar.defaultProps = {
  message: null,
  actionMessage: null,
}
