import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import { PropTypes } from 'prop-types'
import modifyParams from './routerHelpers/modifyParams'

import { idType } from './propType'

import Dropdown from './Dropdown'

// TODO Put these somewhere...
const weekOptionNumbers = []
for (let i = 1; i <= 18; i += 1) {
  weekOptionNumbers.push(i)
}

const valueToText = (week) =>
  `${week < 0 ? 'Preseason' : ''}  Week ${Math.abs(week)}`.trim()

const weekOptions = weekOptionNumbers.map((week) => ({
  label: valueToText(week),
  value: week,
}))

function WeekSelect({ weekId, location }) {
  return (
    <Dropdown
      value={weekId}
      options={weekOptions}
      menuItemProps={(option) => ({
        component: Link,
        to: {
          search: modifyParams(location.search, {
            weekId: option.value,
          }),
        },
      })}
    />
  )
}

WeekSelect.propTypes = {
  weekId: idType.isRequired,
  location: PropTypes.shape({ search: PropTypes.string }).isRequired,
}

export default withRouter(WeekSelect)
