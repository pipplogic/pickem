import {
  getGameById,
  getPickScore,
  getScoreOptions,
} from '../reducers/selectors'

import { updatePickPoints } from '../reducers/actionCreators'

export const mapState = (state, { gameId, poolId }) => {
  const game = getGameById(state)(gameId)

  const score = getPickScore(state)(poolId)(gameId) || ''

  const options = getScoreOptions(state)(poolId)(game.weekId)

  return {
    score,
    options,
  }
}

export const mapDispatch = {
  handleScoreChangeForPool: ({ poolId, gameId }) => (dispatch, getState) => (
    ev,
  ) =>
    updatePickPoints({ gameId, poolId, score: ev.target.value })(
      dispatch,
      getState,
    ),
}
