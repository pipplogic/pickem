import FormControl from '@material-ui/core/FormControl'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Lock from '@material-ui/icons/Lock'
import Check from '@material-ui/icons/Check'
import Clear from '@material-ui/icons/Clear'
import Person from '@material-ui/icons/Person'

import PropTypes from 'prop-types'
import React from 'react'
import { idType } from '../propType'

import {
  CORRECT,
  INCORRECT,
  IN_PROGRESS,
  OPEN,
  OTHER_USER,
  values as pickStatusValues,
} from '../Game/pickStatus'

const getIconForPickStatus = (pickStatus) => {
  switch (pickStatus) {
    case CORRECT:
      return Check
    case INCORRECT:
      return Clear
    case IN_PROGRESS:
      return Lock
    case OTHER_USER:
      return Person
    case OPEN:
    default:
      return undefined
  }
}

const getClassForPickStatus = (classes) => (pickStatus) => {
  switch (pickStatus) {
    case CORRECT:
      return classes.correct
    case INCORRECT:
      return classes.incorrect
    case IN_PROGRESS:
    case OPEN:
    case OTHER_USER:
    default:
      return undefined
  }
}

const styles = (theme) => ({
  optionsMenu: {
    [theme.breakpoints.up('sm')]: {
      transform: 'translateX(100%) !important',
    },
  },
  correct: {
    color: theme.palette.status.success[700],
  },
  incorrect: {
    color: theme.palette.status.error[700],
  },
})

const useStyles = makeStyles(styles)

function PointPicker({
  className,
  score,
  options,
  poolId,
  handleScoreChangeForPool,
  gameId,
  pickStatus,
  locked,
}) {
  const classes = useStyles()
  return (
    <FormControl className={className}>
      <Select
        value={score}
        onChange={handleScoreChangeForPool({ poolId, gameId })}
        IconComponent={getIconForPickStatus(pickStatus)}
        classes={{
          icon: getClassForPickStatus(classes)(pickStatus),
        }}
        disabled={locked}
      >
        {!locked &&
          options.map((option) => (
            <MenuItem key={option} value={option}>
              {option}
            </MenuItem>
          ))}
        {locked && <MenuItem value={score}>{score}</MenuItem>}
      </Select>
    </FormControl>
  )
}

PointPicker.propTypes = {
  className: PropTypes.string,
  gameId: idType.isRequired,
  handleScoreChangeForPool: PropTypes.func.isRequired,
  locked: PropTypes.bool.isRequired,
  poolId: idType.isRequired,
  score: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  options: PropTypes.arrayOf(PropTypes.number).isRequired,
  pickStatus: PropTypes.oneOf(pickStatusValues).isRequired,
}

export default PointPicker
