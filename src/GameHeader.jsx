import Typography from '@material-ui/core/Typography'
import makeStyles from '@material-ui/core/styles/makeStyles'
import React from 'react'

import styles from './Game/styles'

const useStyles = makeStyles(styles)

export default function GameHeader() {
  const classes = useStyles()
  return (
    <div className={classes.game}>
      <Typography>Time</Typography>
      <Typography>Away</Typography>
      <Typography />
      <Typography>Home</Typography>
      <Typography>Pts</Typography>
    </div>
  )
}
