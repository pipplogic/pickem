import { combineReducers } from 'redux'
import {
  loadPools as apiLoadPools,
  loadPoolInvites as apiLoadPoolInvites,
  joinPool as apiJoinPool,
} from '../api'

const ADD_POOLS = 'pickem/pools/add'
const SWITCH_POOL = 'pickem/pools/active'
const SCORING_OPTIONS = 'pcikem/pools/options'
const SET_INVITES = 'pickem/pools/invites/set'
const REMOVE_INVITE = 'pickem/pools/invites/remove'

// Exported just for testing
export const getPoolWeekId = ({ poolId, weekId }) =>
  `WEEK_${weekId}_POOL_${poolId}`

const poolsReducer = (state = {}, action = {}) => {
  switch (action.type) {
    case ADD_POOLS: {
      const newPools = action.pools.reduce(
        (pools, pool) => ({
          ...pools,
          [pool.poolId]: pool,
        }),
        {},
      )
      return {
        ...state,
        ...newPools,
      }
    }
    default:
      return state
  }
}

const active = (state = null, action = {}) => {
  switch (action.type) {
    case SWITCH_POOL: {
      return action.poolId
    }
    default: {
      return state
    }
  }
}

const scoringOptions = (state = {}, action = {}) => {
  switch (action.type) {
    case SCORING_OPTIONS:
      return {
        ...state,
        [getPoolWeekId(action)]: action.options,
      }
    default:
      return state
  }
}

const invitesReducer = (state = [], action = {}) => {
  switch (action.type) {
    case SET_INVITES: {
      return action.payload
    }
    case REMOVE_INVITE: {
      return state.filter((invite) => invite.poolId !== action.poolId)
    }
    default: {
      return state
    }
  }
}

const setInvites = (payload) => ({
  type: SET_INVITES,
  payload,
})

export default combineReducers({
  byId: poolsReducer,
  active,
  scoringOptions,
  invites: invitesReducer,
})

export const getInvites = (state) => state.invites

export const getPool = (state) => (poolId) => state.byId[poolId]

export const getPoolName = (state) => (poolId) =>
  (getPool(state)(poolId) || {}).poolName

export const getPools = (state) => Object.values(state.byId)

export const getScoringOptions = (state) => (poolId) => (weekId) =>
  state.scoringOptions[getPoolWeekId({ poolId, weekId })] || []

export const getActivePool = (state) => state.active

const addPools = (pools) => ({
  type: ADD_POOLS,
  pools,
})

export const selectPool = (poolId) => ({
  type: SWITCH_POOL,
  poolId,
})

export const setScoreOptions = ({ poolId, weekId, options }) => ({
  type: SCORING_OPTIONS,
  poolId,
  weekId,
  options,
})

export const loadPools = () => (dispatch) => {
  apiLoadPoolInvites().then((invites) => {
    dispatch(setInvites(invites))
  })
  return apiLoadPools().then((pools) => {
    dispatch(addPools(pools))
    if (pools.length === 0) {
      return null
    }
    const defaultPool = pools[0].poolId
    dispatch(selectPool(defaultPool))
    return defaultPool
  })
}

export const joinPool = (poolId) => (dispatch) => () => {
  apiJoinPool(poolId).then(() => {
    dispatch({ type: REMOVE_INVITE, poolId })
    apiLoadPools().then((pools) => {
      dispatch(addPools(pools))
    })
    dispatch(selectPool(poolId))
  })
}
