import * as fromPicks from './picksDuck'
import * as fromPickPoints from './pickPoints'
import * as fromPools from './pools'
import * as fromStandings from './standingsDuck'
import * as fromUsers from './users'
import * as fromSnackbar from './snackbar'

export const addPicks = fromPicks.addPicks
export const updateTeamPick = fromPicks.updateTeamPick

export const updatePickPoints = fromPickPoints.updatePickPoints

export const joinPool = fromPools.joinPool
export const loadPools = fromPools.loadPools
export const selectPool = fromPools.selectPool
export const setScoreOptions = fromPools.setScoreOptions

export const loadStandings = fromStandings.loadStandings
export const sortStandingsBy = fromStandings.sortBy

export const loadUser = fromUsers.loadUser
export const updateSelf = fromUsers.updateSelf
export const addGrantedUsers = fromUsers.addGrantedUsers

export const snackClose = fromSnackbar.close
export const snackExit = fromSnackbar.exit
export const snackAction = fromSnackbar.action

export const addSnack = fromSnackbar.add
