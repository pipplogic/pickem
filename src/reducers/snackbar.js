const ADD = 'pickem/snackbar/add'
const CLOSE = 'pickem/snackbar/close'
const EXIT = 'pickem/snackbar/exit'

export default (
  state = {
    messages: [],
    open: false,
  },
  action = {},
) => {
  switch (action.type) {
    case ADD: {
      return {
        ...state,
        messages: [
          ...state.messages,
          { ...action.message, key: new Date().getTime() },
        ],
        open: !state.open,
      }
    }
    case CLOSE: {
      return { ...state, open: false }
    }
    case EXIT: {
      return {
        ...state,
        open: state.messages.length > 1,
        messages: state.messages.slice(1),
      }
    }
    default:
      return state
  }
}

export const getCurrent = (state) => ({
  ...state.messages[0],
  isOpen: state.open,
})

export const add = (message) => ({
  type: ADD,
  message,
})

export const close = () => ({
  type: CLOSE,
})
export const exit = () => ({
  type: EXIT,
})
export const action = {}
