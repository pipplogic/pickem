import { combineReducers } from 'redux'
import { loadUser as apiLoadUser } from '../api'

export const SELF = 'pickem/users/self'
export const USERS = 'pickem/users/list'

function addToObj(state = {}, action = {}) {
  return {
    ...state,
    [action.id]: {
      ...state[action.id],
      ...action.value,
    },
  }
}

function addToArraySet(state = [], id = null) {
  if (state.includes(id)) {
    return state
  }

  return [...state, id]
}

const byUsername = (state = {}, action = {}) => {
  switch (action.type) {
    case SELF: {
      return addToObj(state, { id: action.value.username, value: action.value })
    }

    case USERS: {
      return action.values.reduce(
        (accumState, value) =>
          addToObj(accumState, { id: value.username, value }),
        state,
      )
    }

    default: {
      return state
    }
  }
}

const granted = (state = [], action = {}) => {
  switch (action.type) {
    case SELF: {
      return addToArraySet(state, action.value.username)
    }

    case USERS: {
      return action.values
        .filter((user) => user.granted)
        .reduce(
          (accumState, value) => addToArraySet(accumState, value.username),
          state,
        )
    }

    default: {
      return state
    }
  }
}

const self = (state = null, action = {}) => {
  switch (action.type) {
    case SELF: {
      return action.value.username
    }
    default: {
      return state
    }
  }
}

export default combineReducers({
  byUsername,
  self,
  granted,
})

export const getSelf = (state) => state.byUsername[state.self]

export const getUser = (state) => (username) => state.byUsername[username]

export const getUserById = (state) => (userId) =>
  Object.keys(state.byUsername)
    .map((username) => state.byUsername[username])
    .filter((user) => String(user.id) === String(userId))[0]

export const getGrantedUsers = (state) => state.granted.map(getUser(state))

export const updateSelf = (user) => ({
  type: SELF,
  value: user,
})

export const updateUsers = (users) => ({
  type: USERS,
  values: users,
})

export const addGrantedUsers = (users) => ({
  type: USERS,
  values: users.map((user) => ({ ...user, granted: true })),
})

export const loadUser = () => (dispatch) => {
  apiLoadUser().then((user) => {
    dispatch(updateSelf(user))
  })
}
