import * as fromWeek from '../Week/weekDuck'
import * as fromPicks from './picksDuck'
import * as fromPools from './pools'
import * as fromStandings from './standingsDuck'
import * as fromUsers from './users'
import * as fromPickPoints from './pickPoints'
import * as fromSnackbar from './snackbar'

import {
  getUsersState,
  getWeekState,
  getPoolsState,
  getPicksState,
  getStandingsState,
  getSnackbarState,
} from './index'

export const getSelf = (state) => fromUsers.getSelf(getUsersState(state))

export const getUserById = (state) =>
  fromUsers.getUserById(getUsersState(state))

export const getGrantedUsers = (state) =>
  fromUsers.getGrantedUsers(getUsersState(state))

export const getDisplayName = (user) => {
  if (!user) {
    return null
  }
  return `${user.firstName} ${user.lastName}`.trim()
}

export const getTeamById = (state) => (teamId) =>
  fromWeek.getTeamById(getWeekState(state))(teamId)

export const getPools = (state) => fromPools.getPools(getPoolsState(state))

export const getPool = (state) => fromPools.getPool(getPoolsState(state))

export const getPoolName = (state) =>
  fromPools.getPoolName(getPoolsState(state))

export const getPoolInvites = (state) =>
  fromPools.getInvites(getPoolsState(state))

export const getGameById = (state) => fromWeek.getGameById(getWeekState(state))

export const getGameIdsForWeek = (state) =>
  fromWeek.getGameIdsForWeek(getWeekState(state))

export const getPick = (state) => (poolId) => (gameId) =>
  fromPicks.getPick(getPicksState(state))(poolId)(gameId) || { gameId, poolId }

export const getPickScore = (state) => (poolId) => (gameId) =>
  getPick(state)(poolId)(gameId).score

export const getPickTeamId = (state) => (poolId) => (gameId) =>
  getPick(state)(poolId)(gameId).teamId

export const isGameLocked = (state) => (gameId, now = new Date()) => {
  const game = getGameById(state)(gameId) || {}
  return game.gameTime < now
}

export const isSavingPicks = (state) => getPicksState(state).saving

export const arePicksModified = (state) => getPicksState(state).modified

export const picksError = (state) => getPicksState(state).error

export const getScoringOptions = (state) =>
  fromPools.getScoringOptions(getPoolsState(state))

export const getScoreOptions = fromPickPoints.getScoreOptions

export const getStandings = (state) =>
  fromStandings.getStandings(getStandingsState(state))
export const areStandingsLoading = (state) =>
  fromStandings.areStandingsLoading(getStandingsState(state))
export const getStandingsSort = (state) =>
  fromStandings.getSort(getStandingsState(state))

export const getSnack = (state) =>
  fromSnackbar.getCurrent(getSnackbarState(state))
