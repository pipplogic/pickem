/* eslint-env jest  */
import reducer, { getUser, updateSelf, updateUsers } from './users'

describe('usersReducer', () => {
  it('should add self', () => {
    const afterAdd = reducer(
      {},
      updateSelf({
        username: 1,
        name: 'Joe',
      }),
    )
    expect(getUser(afterAdd)(1).name).toEqual('Joe')

    const afterUpdate = reducer(
      afterAdd,
      updateSelf({ username: 1, last: 'Cool' }),
    )

    expect(getUser(afterUpdate)(1).name).toEqual('Joe')
    expect(getUser(afterUpdate)(1).last).toEqual('Cool')
  })

  it('should add collections', () => {
    const afterAdd = reducer(
      {},
      updateUsers([
        {
          username: 1,
          name: 'Joe',
        },
        {
          username: 2,
          name: 'Charlie',
        },
      ]),
    )
    expect(getUser(afterAdd)(1).name).toEqual('Joe')
    expect(getUser(afterAdd)(2).name).toEqual('Charlie')
  })
})
