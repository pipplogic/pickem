import { combineReducers } from 'redux'

import login from '../Login/loginDuck'
import status from './status'

import register from '../Register/registerDuck'
import confirm from '../ConfirmUser/confirmDuck'
import forgotPassword from '../ForgotPassword/forgotPasswordDuck'
import week from '../Week/weekDuck'
import picksDuck from './picksDuck'
import pools from './pools'
import standings from './standingsDuck'
import users from './users'
import snackbar from './snackbar'

export default combineReducers({
  login,
  register,
  confirm,
  forgotPassword,
  week,
  picksDuck,
  status,
  pools,
  standings,
  users,
  snackbar,
})

export const getRegisterState = (state) => state.register
export const getLoginState = (state) => state.login
export const getConfirmState = (state) => state.confirm
export const getForgotPasswordState = (state) => state.forgotPassword
export const getWeekState = (state) => state.week
export const getPicksState = (state) => state.picksDuck
export const getPoolsState = (state) => state.pools
export const getStandingsState = (state) => state.standings
export const getUsersState = (state) => state.users
export const getSnackbarState = (state) => state.snackbar
