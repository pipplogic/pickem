import { combineReducers } from 'redux'
import { loadStandings as apiLoadStandings } from '../api'

const STANDINGS_LOADING = 'pickem/standings/loading'
const STANDINGS_LOADED = 'pickem/standings/loaded'
const STANDINGS_ERROR = 'pickem/standings/error'
const SORT = 'pickem/standings/sort'

const getPoolWeekId = ({ poolId, weekId }) => `WEEK_${weekId}_POOL_${poolId}`

const byId = (state = {}, action = {}) => {
  switch (action.type) {
    case STANDINGS_LOADED: {
      return {
        ...state,
        [getPoolWeekId(action)]: action.scores,
      }
    }
    default:
      return state
  }
}

const loading = (state = [], action = {}) => {
  switch (action.type) {
    case STANDINGS_LOADING: {
      return {
        ...state,
        [getPoolWeekId(action)]: true,
      }
    }
    case STANDINGS_ERROR:
    case STANDINGS_LOADED: {
      return {
        ...state,
        [getPoolWeekId(action)]: false,
      }
    }
    default:
      return state
  }
}

function shouldReverse(state = { col: null, reverse: false }, action = {}) {
  if (action.reverse != null) {
    return action.reverse
  }
  if (action.col === state.col) {
    return !state.reverse
  }
  return false
}

const sort = (state = { col: null, reverse: false }, action = {}) => {
  switch (action.type) {
    case SORT: {
      return {
        col: action.col,
        reverse: shouldReverse(state, action),
      }
    }

    default:
      return state
  }
}
export default combineReducers({ byId, loading, sort })

export const getStandings = (state) => ({ poolId, weekId }) =>
  state.byId[getPoolWeekId({ poolId, weekId })]

export const getSort = (state) => state.sort

export const areStandingsLoading = (state) => ({ poolId, weekId }) => {
  const loadingStandings = state.loading[getPoolWeekId({ poolId, weekId })]
  return loadingStandings === undefined ? true : loadingStandings
}

export const sortBy = (col, opts) => ({
  type: SORT,
  col,
  ...opts,
})

export const loadStandings = ({ poolId, weekId }) => (dispatch) => {
  dispatch({ type: STANDINGS_LOADING, poolId, weekId })
  apiLoadStandings({ poolId, weekId })
    .then((scores) => {
      dispatch({
        type: STANDINGS_LOADED,
        poolId,
        weekId,
        scores,
      })
    })
    .catch((error) => {
      dispatch({ type: STANDINGS_ERROR, error })
    })
}
