import { getGameById, isGameLocked, getPickTeamId } from '../reducers/selectors'

import { updateTeamPick } from '../reducers/actionCreators'

import { getPickStatus } from './pickStatus'

export const mapState = (state, { gameId, poolId, userId }) => {
  const game = getGameById(state)(gameId)
  const locked = isGameLocked(state)(gameId) || Boolean(userId)
  const selectedTeam = getPickTeamId(state)(poolId)(gameId)

  const complete = game.complete
  const correct = game.winningTeam === selectedTeam

  const pickStatus = getPickStatus({
    complete,
    correct,
    locked,
    otherUser: Boolean(userId),
  })
  return {
    game,
    locked,
    poolId,
    selectedTeam,
    pickStatus,
  }
}

export const mapDispatch = {
  selectTeam: ({ poolId, gameId, teamId }) => (dispatch) => () =>
    dispatch(updateTeamPick({ poolId, gameId, teamId })),
}
