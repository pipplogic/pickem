import { connect } from 'react-redux'
import Game from './Game'
import { mapState, mapDispatch } from './reduxMappers'

export default connect(
  mapState,
  mapDispatch,
)(Game)
