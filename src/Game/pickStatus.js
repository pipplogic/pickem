export const OPEN = 'OPEN'
export const IN_PROGRESS = 'IN_PROGRESS'
export const INCORRECT = 'INCORRECT'
export const CORRECT = 'CORRECT'
export const OTHER_USER = 'OTHER_USER'

export const getPickStatus = ({ locked, complete, correct, otherUser }) => {
  if (complete) {
    return correct ? CORRECT : INCORRECT
  }

  if (otherUser) {
    return OTHER_USER
  }

  if (locked) {
    return IN_PROGRESS
  }

  return OPEN
}

export const values = [OPEN, IN_PROGRESS, INCORRECT, CORRECT, OTHER_USER]
