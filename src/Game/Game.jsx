import Typography from '@material-ui/core/Typography'
import makeStyles from '@material-ui/core/styles/makeStyles'
import cx from 'classnames'
import PropTypes from 'prop-types'
import React from 'react'

import DateTime from '../DateTime'
import Team from '../Team'
import { idType } from '../propType'
import PointPicker from '../PointPicker'

import { values as pickStatusValues } from './pickStatus'
import styles from './styles'

const useStyles = makeStyles(styles)

export default function Game({
  game,
  poolId,
  locked,
  selectedTeam,
  selectTeam,
  pickStatus,
}) {
  const classes = useStyles()
  return (
    <div
      className={cx(classes.game, {
        [classes.locked]: locked,
      })}
    >
      <DateTime className={classes.time} date={game.gameTime} />
      <Team
        gameId={game.gameId}
        className={classes.away}
        teamId={game.awayTeam}
        locked={locked}
        selected={selectedTeam === game.awayTeam}
        onClick={selectTeam({
          gameId: game.gameId,
          poolId,
          teamId: game.awayTeam,
        })}
      />
      <Typography className={classes.at}>@</Typography>
      <Team
        gameId={game.gameId}
        className={classes.home}
        teamId={game.homeTeam}
        locked={locked}
        selected={selectedTeam === game.homeTeam}
        onClick={selectTeam({
          gameId: game.gameId,
          poolId,
          teamId: game.homeTeam,
        })}
      />
      <div className={classes.pts}>
        <PointPicker
          gameId={game.gameId}
          poolId={poolId}
          pickStatus={pickStatus}
          locked={locked}
          correct={game.winningTeam === selectedTeam}
        />
      </div>
    </div>
  )
}

Game.propTypes = {
  game: PropTypes.shape({
    awayTeam: idType.isRequired,
    gameId: idType.isRequired,
    gameTime: PropTypes.number.isRequired,
    homeTeam: idType.isRequired,
    winningTeam: idType,
    complete: PropTypes.bool.isRequired,
  }).isRequired,
  selectedTeam: idType,
  locked: PropTypes.bool.isRequired,
  poolId: idType.isRequired,
  selectTeam: PropTypes.func.isRequired,
  pickStatus: PropTypes.oneOf(pickStatusValues).isRequired,
}

Game.defaultProps = {
  selectedTeam: null,
}
