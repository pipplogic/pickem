import Typography from '@material-ui/core/Typography'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Autorenew from '@material-ui/icons/Autorenew'
import Error from '@material-ui/icons/Error'
import cx from 'classnames'
import PropTypes from 'prop-types'
import React from 'react'
import styles from './styles'

const statusToIcon = {
  loading: Autorenew,
  error: Error,
}

const statusToColor = {
  error: 'error',
}

const statusToSpin = {
  loading: true,
}

const statusToText = {
  error: 'Error',
  loading: 'Loading',
}

const useStyles = makeStyles(styles)

export default function StatusPage({
  className,
  children,
  status,
  Icon = statusToIcon[status],
  iconColor = statusToColor[status] || 'primary',
  iconSpin = statusToSpin[status],
  iconClassName,
  text = statusToText[status],
}) {
  const classes = useStyles()
  return (
    <div className={cx(className, classes.root)}>
      {Icon && (
        <Icon
          color={iconColor}
          classes={{
            root: cx(iconClassName, classes.icon, { [classes.spin]: iconSpin }),
          }}
        />
      )}
      {children && <Typography variant="h5">{children}</Typography>}
      {!children && text && <Typography variant="h5">{text}</Typography>}
    </div>
  )
}

StatusPage.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  // React.ComponentType
  Icon: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  iconColor: PropTypes.string,
  iconSpin: PropTypes.bool,
  status: PropTypes.oneOf(Object.keys(statusToIcon)),
  text: PropTypes.string,
  iconClassName: PropTypes.string,
}

StatusPage.defaultProps = {
  className: '',
  children: null,
}
