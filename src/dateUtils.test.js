import { currentWeek, currentSeason } from './dateUtils'

describe('date', () => {
  it('2019 week 2 monday', () => {
    const now = new Date(2019, 8, 16)
    expect(currentSeason(now)).toBe(2019)
    expect(currentWeek(now)).toBe(2)
  })
  it('2019 week 2 tues after', () => {
    const now = new Date(2019, 8, 17)
    expect(currentSeason(now)).toBe(2019)
    expect(currentWeek(now)).toBe(2)
  })
  it('2019 week 3 wed before', () => {
    const now = new Date(2019, 8, 18)
    expect(currentSeason(now)).toBe(2019)
    expect(currentWeek(now)).toBe(3)
  })
  it('2019 week 4 thurs', () => {
    const now = new Date(2019, 8, 19)
    expect(currentSeason(now)).toBe(2019)
    expect(currentWeek(now)).toBe(3)
  })

  it('2019 week 4', () => {
    const now = new Date(2019, 8, 27)
    expect(currentSeason(now)).toBe(2019)
    expect(currentWeek(now)).toBe(4)
  })

  it('2020 week 1', () => {
    const now = new Date(2020, 8, 9)
    expect(currentSeason(now)).toBe(2020)
    expect(currentWeek(now)).toBe(1)
  })

  it('2020 week 1 monday', () => {
    const now = new Date(2020, 8, 14)
    expect(currentSeason(now)).toBe(2020)
    expect(currentWeek(now)).toBe(1)
  })

  it('2021 week 1 monday', () => {
    const now = new Date(2021, 8, 13)
    expect(currentSeason(now)).toBe(2021)
    expect(currentWeek(now)).toBe(1)
  })

  it('2021 week 1 tues', () => {
    const now = new Date(2021, 8, 14)
    expect(currentSeason(now)).toBe(2021)
    expect(currentWeek(now)).toBe(1)
  })

  it('2021 week 2 wed before', () => {
    const now = new Date(2021, 8, 15)
    expect(currentSeason(now)).toBe(2021)
    expect(currentWeek(now)).toBe(2)
  })
})
