import Typography from '@material-ui/core/Typography'
import format from 'date-fns/format'
import PropTypes from 'prop-types'
import React from 'react'
import Hidden from '@material-ui/core/Hidden'

export default function DateTime({ date, className }) {
  date = new Date(date)
  const weekday = format(date, 'E MMM d')
  const smallWeekday = format(date, 'E')
  const time = format(date, 'hh:mm a')
  const smallTime = format(date, 'h:mm')

  return (
    <div className={className}>
      <Hidden implementation="css" mdUp>
        <Typography>{smallWeekday}</Typography>
        <Typography>{smallTime}</Typography>
      </Hidden>
      <Hidden implementation="css" smDown>
        <Typography>{weekday}</Typography>
        <Typography>{time}</Typography>
      </Hidden>
    </div>
  )
}

DateTime.propTypes = {
  date: PropTypes.number.isRequired,
  className: PropTypes.string,
}

DateTime.defaultProps = {
  className: '',
}
