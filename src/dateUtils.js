import addDays from 'date-fns/addDays'
import setDay from 'date-fns/setDay'
import differenceInDays from 'date-fns/differenceInDays'

export function currentSeason(time = new Date()) {
  const year = time.getFullYear()
  const month = time.getMonth() + 1
  if (month < 7) {
    return year - 1
  }
  return year
}

export function currentWeek(now = new Date()) {
  // Week 1
  // 2021 - Thur 9/9
  // 2020 - Thur 9/10
  // 2019 - Thur 9/5
  // 2018 - Thur 9/6
  // 2017 - Thur 9/7
  // 2016 - Thur 9/8
  // 2015 - Thur 9/10
  // 2014 - Thur 9/4
  const currentYear = currentSeason(now)
  const earliestPossibleWednesday = new Date(currentYear, 8, 4)
  let week1Wednesday = setDay(earliestPossibleWednesday, 3)
  if (week1Wednesday < earliestPossibleWednesday) {
    week1Wednesday = addDays(week1Wednesday, 7)
  }

  const daysSinceStart = differenceInDays(now, week1Wednesday)
  const weeksSinceStart = Math.floor(daysSinceStart / 7)

  let weekNum = weeksSinceStart + 1

  return Math.max(1, Math.min(weekNum, 18))
}
