import AppBar from '@material-ui/core/AppBar'
import Typography from '@material-ui/core/Typography'
import PropTypes from 'prop-types'
import React from 'react'
import { makeStyles } from '@material-ui/core/styles'

const styles = (theme) => ({
  root: {
    backgroundColor: theme.palette.primary.dark,
    color: theme.palette.getContrastText(theme.palette.primary.dark),
    padding: `${theme.spacing(1)}px 0`,
  },
})

const useStyles = makeStyles(styles)

export default function Footer({ className }) {
  const classes = useStyles()

  return (
    <AppBar
      component="footer"
      position="static"
      classes={{ root: classes.root }}
      className={className}
    >
      <Typography variant="subtitle1" align="center">
        Written by @PippLogic and @quadglacier
      </Typography>
    </AppBar>
  )
}

Footer.propTypes = {
  className: PropTypes.string,
}

Footer.defaultProps = {
  className: '',
}
