import { connect } from 'react-redux'
import Login from './Login'

import { buildActionCreators } from './loginDuck'
import { getLoginState } from '../reducers'

export default connect(
  getLoginState,
  buildActionCreators(getLoginState),
)(Login)
