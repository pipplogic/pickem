import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Typography from '@material-ui/core/Typography'
import Autorenew from '@material-ui/icons/Autorenew'
import Send from '@material-ui/icons/Send'
import cx from 'classnames'
import PropTypes from 'prop-types'
import React from 'react'
import { Link } from 'react-router-dom'
import styles from './styles'

const useStyles = makeStyles(styles)

export default function Login({
  error,
  handleInputChange,
  handleSubmit,
  pass,
  submitting,
  user,
}) {
  const classes = useStyles()
  return (
    <form className={classes.root} noValidate onSubmit={handleSubmit}>
      <Typography variant="subtitle1" align="center">
        Login
      </Typography>
      <TextField
        label="Email"
        name="user"
        value={user}
        onChange={handleInputChange}
        disabled={submitting}
        error={error}
      />
      <TextField
        label="Password"
        type="password"
        name="pass"
        value={pass}
        onChange={handleInputChange}
        disabled={submitting}
        error={error}
      />
      <Button
        type="submit"
        variant="contained"
        color="primary"
        disabled={submitting}
      >
        Login
        {submitting && (
          <Autorenew className={cx(classes.rightIcon, classes.load)} />
        )}
        {!submitting && <Send className={classes.rightIcon} />}
      </Button>
      <div>
        <Typography>
          <Link to="/register">Register Here</Link>
        </Typography>
        <Typography>
          <Link to="/forgot-password">Forgot Password?</Link>
        </Typography>
      </div>
    </form>
  )
}

Login.propTypes = {
  submitting: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
  pass: PropTypes.string.isRequired,
  user: PropTypes.string.isRequired,
  handleInputChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
}
