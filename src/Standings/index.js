import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import Standings from './Standings'

import {
  areStandingsLoading,
  getStandings,
  getSelf,
  getGrantedUsers,
  getStandingsSort,
} from '../reducers/selectors'

import { loadStandings, sortStandingsBy } from '../reducers/actionCreators'

import columns from './columns'
import { currentWeek } from '../dateUtils'

const mapState = (state, { poolId, weekId = String(currentWeek()) }) => {
  const loading = areStandingsLoading(state)({ poolId, weekId })
  const standings = getStandings(state)({ poolId, weekId }) || []
  const username = (getSelf(state) || {}).username
  const sort = getStandingsSort(state)
  const sortDirection = sort.reverse ? 'asc' : 'desc'
  const sortCol = columns.find((col) => col.id === sort.col) || {}

  const grantedUsers = getGrantedUsers(state)

  const getSortValue = sortCol.getSortValue || sortCol.getValue

  const sortedStandings = [...standings]
    .sort((lhs, rhs) => rhs.ytdScore - lhs.ytdScore)
    .map((standing, idx) => ({
      ...standing,
      place: idx + 1,
    }))
    .sort((lhs, rhs) => {
      const leftValue = getSortValue(lhs)
      const rightValue = getSortValue(rhs)

      let comparison
      if (sortCol.numeric) {
        comparison = rightValue - leftValue
      } else {
        comparison = leftValue.localeCompare(rightValue)
      }
      if (sort.reverse) {
        comparison = -comparison
      }
      return comparison
    })

  return {
    poolId,
    loading,
    standings: sortedStandings,
    username,
    sortDirection,
    sortCol,
    grantedUsers,
  }
}

const mapDispatch = { loadStandings, sortStandingsBy }

export default withRouter(
  connect(
    mapState,
    mapDispatch,
  )(Standings),
)
