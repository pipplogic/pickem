export default [
  {
    id: 'place',
    numeric: true,
    label: 'Place',
    getValue: (row) => row.place,
  },
  {
    id: 'name',
    label: 'Name',
    getSortValue: (row) => row.lastName,
    getValue: (row) => `${row.firstName} ${row.lastName}`,
  },
  {
    id: 'weekScore',
    numeric: true,
    labelForProps: ({ weekId }) => `Week ${weekId}`,
    getValue: (row) => row.weekScore,
  },
  {
    id: 'yearScore',
    numeric: true,
    label: 'Year',
    getValue: (row) => row.ytdScore,
  },
]
