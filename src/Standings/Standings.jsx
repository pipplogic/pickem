import React, { useEffect } from 'react'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import Tooltip from '@material-ui/core/Tooltip'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Assignment from '@material-ui/icons/Assignment'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import { idType } from '../propType'
import StatusPage from '../StatusPage'
import columns from './columns'
import modifyParams from '../routerHelpers/modifyParams'
import styles from './styles'

const useStyles = makeStyles(styles)

export default function Standings({
  className,
  grantedUsers,
  loading,
  loadStandings,
  location,
  poolId,
  sortCol,
  standings,
  sortDirection,
  sortStandingsBy,
  username,
  weekId,
}) {
  const classes = useStyles()

  useEffect(() => {
    sortStandingsBy('yearScore', { reverse: false })
  }, [])
  useEffect(() => {
    if (!poolId || !weekId) {
      return
    }
    loadStandings({ poolId, weekId })
  }, [poolId, weekId])

  if (loading) {
    return <StatusPage className={className} status="loading" />
  }
  return (
    <div className={className}>
      <Table>
        <TableHead>
          <TableRow>
            {columns.map((col) => (
              <TableCell
                key={col.id}
                align={col.numeric ? 'right' : 'left'}
                className={classes.head}
                style={{ whiteSpace: 'nowrap' }}
                sortDirection={sortCol.id === col.id ? sortDirection : false}
              >
                <Tooltip
                  title="Sort"
                  placement={col.numeric ? 'bottom-end' : 'bottom-start'}
                  enterDelay={300}
                >
                  <TableSortLabel
                    active={sortCol.id === col.id}
                    direction={sortDirection}
                    onClick={() => sortStandingsBy(col.id)}
                  >
                    {col.labelForProps && col.labelForProps({ weekId })}
                    {col.label}
                  </TableSortLabel>
                </Tooltip>
              </TableCell>
            ))}
            {grantedUsers.length > 1 && <TableCell className={classes.head} />}
          </TableRow>
        </TableHead>
        <TableBody>
          {standings.map((row) => {
            const isOwnRow = row.username === username
            return (
              <TableRow
                key={row.username}
                selected={isOwnRow}
                classes={{ selected: classes.selected }}
              >
                {columns.map((col) => (
                  <TableCell
                    key={col.id}
                    align={col.numeric ? 'right' : 'left'}
                    className={isOwnRow ? classes.selected : null}
                  >
                    {col.getValue(row)}
                  </TableCell>
                ))}
                {grantedUsers.length > 1 && (
                  <TableCell>
                    {grantedUsers
                      .filter(
                        (grantedUser) => grantedUser.username === row.username,
                      )
                      .map((grantedUser) => (
                        <Link
                          key={grantedUser.id}
                          to={{
                            pathname: '/picks',
                            search: modifyParams(location.search, {
                              userId: grantedUser.id,
                            }),
                          }}
                        >
                          <Assignment />
                        </Link>
                      ))}
                  </TableCell>
                )}
              </TableRow>
            )
          })}
        </TableBody>
      </Table>
    </div>
  )
}

Standings.propTypes = {
  poolId: idType,
  weekId: idType,
  loadStandings: PropTypes.func.isRequired,
  standings: PropTypes.arrayOf(
    PropTypes.shape({
      username: idType.isRequired,
      firstName: PropTypes.string.isRequired,
      lastName: PropTypes.string.isRequired,
      score: PropTypes.number,
    }),
  ).isRequired,
  loading: PropTypes.bool.isRequired,
  className: PropTypes.string,
  username: PropTypes.string,
  sortStandingsBy: PropTypes.func.isRequired,
  sortCol: PropTypes.shape({
    id: PropTypes.oneOf(columns.map((column) => column.id)),
  }),
  sortDirection: PropTypes.oneOf(['asc', 'desc']),
  grantedUsers: PropTypes.arrayOf(
    PropTypes.shape({
      username: PropTypes.string.isRequired,
      id: idType.isRequired,
    }).isRequired,
  ).isRequired,
  location: PropTypes.shape({
    search: PropTypes.string,
    pathname: PropTypes.string,
  }).isRequired,
}
