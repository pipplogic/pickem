import { stickyTop } from '../styleHelpers'

export default (theme) => ({
  head: {
    backgroundColor: theme.palette.grey[200],
    ...stickyTop(theme),
  },
  selected: {
    fontSize: theme.typography.fontSize,
    fontWeight: theme.typography.fontWeightMedium,
  },
})
