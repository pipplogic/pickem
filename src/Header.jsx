import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import makeStyles from '@material-ui/core/styles/makeStyles'
import React from 'react'
import FootballHelmet from 'mdi-material-ui/FootballHelmet'

const styles = (theme) => ({
  title: {
    flexGrow: 1,
  },
  logo: {
    transform: 'scaleX(-1)',
    marginRight: theme.spacing(1),
  },
})

const useStyles = makeStyles(styles)

export default function Header() {
  const classes = useStyles()
  // TODO Sticky
  return (
    <AppBar position="static">
      <Toolbar>
        <FootballHelmet className={classes.logo} />
        <Typography variant="h6" className={classes.title}>
          Pick &apos;Em
        </Typography>
      </Toolbar>
    </AppBar>
  )
}
