import axios from 'axios'
import { currentSeason } from './dateUtils'

const seasonId = currentSeason()

class BadAuthentication extends Error {
  constructor(msg) {
    super(msg)
    this.name = 'BadAuthentication'
  }
}

function setToken(token) {
  if (token) {
    const authToken = `Bearer ${token}`
    axios.defaults.headers.common.Authorization = authToken
    window.localStorage.setItem('token', token)
  } else {
    axios.defaults.headers.common.Authorization = undefined
    window.localStorage.removeItem('token')
  }
}

export function loadToken() {
  const authToken = window.localStorage.getItem('token')
  setToken(authToken)
  return authToken
}

export function logout() {
  setToken(null)
  window.location.assign('/')
}

axios.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error.response.status === 401) {
      logout()
      throw new BadAuthentication('User not authenticated')
    }
    throw error
  },
)

export function loadWeek(week) {
  return axios
    .get(`/api/v1/games/season/${seasonId}/week/${week}`)
    .then(({ data }) => ({
      ...data,
      games: data.games.map((game) => ({
        ...game,
        weekId: week,
        homeTeam: game.homeTeam.teamId,
        awayTeam: game.awayTeam.teamId,
        winningTeam: (game.winningTeam || {}).teamId,
        gameTime: game.gameTimeEpoch,
        complete: game.gameComplete,
      })),
      teams: data.games
        .map((game) => [game.homeTeam, game.awayTeam])
        .reduce((teams, gameTeams) => [...teams, ...gameTeams], []),
    }))
}

export const registerUser = (payload) => axios.post('/api/register', payload)

export const forgotPasswordForUser = (payload) =>
  axios.post('/api/users/forgot-password', payload)

export function login({ user, pass }) {
  return axios
    .post('/api/login', { username: user, password: pass })
    .then((resp) => {
      setToken(resp.data)
    })
}

export function confirmUser(payload) {
  return axios.post('/api/users/confirm', payload)
}

export function loadPools() {
  return axios.get('/api/v1/pool/list').then((resp) => resp.data.poolViews)
}

export function loadPoolInvites() {
  return axios
    .get('/api/v1/pool/invite')
    .then((resp) => resp.data.filter((invite) => invite.status === 'PENDING'))
}

export function loadPoolOptions({ poolId, weekId = 1 }) {
  return axios
    .get(
      `/api/v1/picks/values?poolId=${poolId}&season=${seasonId}&week=${weekId}`,
    )
    .then(({ data }) => data)
}

export function loadPicks({ poolId, weekId, userId }) {
  let url
  if (userId != null) {
    url = `/api/v1/picks/user/${userId}/pool/${poolId}/season/${seasonId}/week/${weekId}`
  } else {
    url = `/api/v1/picks/pool/${poolId}/season/${seasonId}/week/${weekId}`
  }

  return axios.get(url).then(({ data }) =>
    data.map(({ gameId, chosenTeamId, confidence }) => ({
      gameId,
      teamId: chosenTeamId,
      score: confidence,
      poolId,
      userId,
    })),
  )
}

export function savePicks({ poolId, picks }) {
  const gamePicks = picks
    .filter((game) => game.gameId)
    .map(({ teamId, gameId, score }) => ({
      gameId,
      chosenTeamId: teamId,
      confidence: score,
    }))

  const payload = { poolId, gamePicks }

  return axios.post('/api/v1/picks', payload)
}

export function getGrantedUsers(poolId) {
  return axios
    .get(`/api/v1/user/grant?poolId=${poolId}`)
    .then(({ data }) => data)
}

export function joinPool(poolId) {
  return axios.put(`/api/pools/${poolId}`, { joined: true })
}

export function loadStandings({ poolId, weekId }) {
  return axios
    .get(
      `/api/v1/score/week?season=${seasonId}&week=${weekId}&poolId=${poolId}`,
    )
    .then(({ data }) => data.userScores || [])
}

export function loadUser() {
  return axios.get('/api/v1/user').then(({ data }) => data)
}
