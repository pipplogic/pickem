import { connect } from 'react-redux'
import Register from './Register'
import { buildActionCreators } from './registerDuck'
import { getRegisterState } from '../reducers'

export default connect(
  getRegisterState,
  buildActionCreators(getRegisterState),
)(Register)
