import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Autorenew from '@material-ui/icons/Autorenew'
import MailOutline from '@material-ui/icons/MailOutline'
import Send from '@material-ui/icons/Send'
import PropTypes from 'prop-types'
import React from 'react'
import StatusPage from '../StatusPage'
import styles from './styles'

const useStyles = makeStyles(styles)

export default function Register({
  first,
  last,
  email,
  error,
  submitting,
  success,
  handleInputChange,
  handleSubmit,
}) {
  const classes = useStyles()
  if (success) {
    return (
      <StatusPage
        className={classes.root}
        Icon={MailOutline}
        text="Check your email"
      />
    )
  }
  return (
    <form className={classes.root} noValidate onSubmit={handleSubmit}>
      <Typography variant="subtitle1" align="center">
        Register
      </Typography>
      <TextField
        label="First Name"
        name="first"
        value={first}
        error={Boolean(error)}
        onChange={handleInputChange}
      />
      <TextField
        label="Last Name"
        name="last"
        value={last}
        error={Boolean(error)}
        disabled={submitting}
        onChange={handleInputChange}
      />
      <TextField
        type="email"
        label="Email"
        name="email"
        value={email}
        error={Boolean(error)}
        disabled={submitting}
        onChange={handleInputChange}
      />

      <Button
        type="submit"
        variant="contained"
        color="primary"
        disabled={submitting}
      >
        Register
        {submitting && (
          <Autorenew className={`${classes.rightIcon} ${classes.load}`} />
        )}
        {!submitting && <Send className={classes.rightIcon} />}
      </Button>
    </form>
  )
}

Register.propTypes = {
  email: PropTypes.string.isRequired,
  error: PropTypes.string,
  first: PropTypes.string.isRequired,
  handleInputChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  last: PropTypes.string.isRequired,
  submitting: PropTypes.bool.isRequired,
  success: PropTypes.bool.isRequired,
}
