import { connect } from 'react-redux'

import ForgotPassword from './ForgotPassword'
import { buildActionCreators } from './forgotPasswordDuck'
import { getForgotPasswordState } from '../reducers'

export default connect(
  getForgotPasswordState,
  buildActionCreators(getForgotPasswordState),
)(ForgotPassword)
