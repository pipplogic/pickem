import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import qs from 'qs'
import {
  getSelf,
  getPools,
  getPoolInvites,
  getPoolName,
  getUserById,
  getGrantedUsers,
} from '../reducers/selectors'

import {
  loadPools,
  joinPool,
  loadUser,
  addGrantedUsers,
} from '../reducers/actionCreators'

import LoggednInView from './LoggedInView'

const mapState = (state, props) => {
  const params = qs.parse(props.location.search, { ignoreQueryPrefix: true })
  const { poolId, weekId, userId } = params

  const pools = getPools(state)
  const invites = getPoolInvites(state)
  const poolName = getPoolName(state)(poolId)
  const grantedUsers = getGrantedUsers(state)
  const self = getSelf(state)

  const isSelf = userId && self && String(self.id) === String(userId)
  const viewingUser = isSelf ? null : getUserById(state)(userId)

  return {
    self,
    viewingUser,
    grantedUsers,
    pools,
    invites,
    poolName,
    poolId,
    weekId,
  }
}

const mapDispatch = {
  loadPools,
  loadUser,
  joinPool,
  addGrantedUsers,
}

export default withRouter(
  connect(
    mapState,
    mapDispatch,
  )(LoggednInView),
)
