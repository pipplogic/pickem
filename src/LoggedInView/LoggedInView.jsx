import React, { Fragment, useState, useEffect } from 'react'
import classNames from 'classnames'
import Drawer from '@material-ui/core/Drawer'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import List from '@material-ui/core/List'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
import IconButton from '@material-ui/core/IconButton'
import makeStyles from '@material-ui/core/styles/makeStyles'
import MenuIcon from '@material-ui/icons/Menu'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Button from '@material-ui/core/Button'
import Hidden from '@material-ui/core/Hidden'
import PropTypes from 'prop-types'
import FootballHelmet from 'mdi-material-ui/FootballHelmet'
import FormatListNumbered from '@material-ui/icons/FormatListNumbered'
import Add from '@material-ui/icons/Add'
import Assignment from '@material-ui/icons/Assignment'
import { Link, Route, Switch, Redirect } from 'react-router-dom'
import { logout, getGrantedUsers } from '../api'
import { idType } from '../propType'
import WeekSelect from '../WeekSelect'
import Week from '../Week'
import Standings from '../Standings'
import UserSelect from '../UserSelect'
import modifyParams from '../routerHelpers/modifyParams'
import { getDisplayName } from '../reducers/selectors'
import styles from './styles'
import { currentWeek } from '../dateUtils'

export const MyFootballHelmet = (props) => (
  <FootballHelmet style={{ transform: 'scaleX(-1)' }} {...props} />
)

const useStyles = makeStyles(styles)

export default function LoggedInView({
  addGrantedUsers,
  grantedUsers,
  history,
  invites,
  joinPool,
  loadPools,
  loadUser,
  location,
  poolId,
  poolName,
  pools,
  self,
  viewingUser,
  weekId = String(currentWeek()),
}) {
  const classes = useStyles()
  const [open, setOpen] = useState(false)

  useEffect(() => {
    loadPools().then((defaultPoolId) => {
      if (poolId) {
        return
      }
      history.push({
        search: modifyParams(location.search, {
          poolId: defaultPoolId,
          userId: undefined,
        }),
      })
    })
    loadUser()
  }, [])

  useEffect(() => {
    function updateVhVar() {
      const innerHeight = window.innerHeight
      document.documentElement.style.setProperty(
        '--innerHeight',
        `${innerHeight}px`,
      )
    }
    updateVhVar()
    window.addEventListener('resize', updateVhVar)
    window.addEventListener('orientationchange', updateVhVar)

    return () => {
      window.removeEventListener('resize', updateVhVar)
      window.removeEventListener('orientationchange', updateVhVar)
    }
  }, [])

  useEffect(() => {
    if (poolId == null) {
      return
    }
    getGrantedUsers(poolId).then((users) => {
      addGrantedUsers(users)
    })
  }, [poolId])

  const drawerContent = (
    <Fragment>
      <div className={classes.toolbarIcon}>
        <IconButton onClick={() => setOpen(false)}>
          <ChevronLeftIcon />
        </IconButton>
      </div>
      <Divider />
      {invites.map((invite) => (
        <Fragment key={invite.poolId}>
          <List>
            <ListItem button onClick={joinPool(invite.poolId)}>
              <ListItemIcon>
                <Add />
              </ListItemIcon>
              <ListItemText primary={invite.poolName} />
            </ListItem>
          </List>
          <Divider />
        </Fragment>
      ))}
      {pools.map((pool) => (
        <Fragment key={pool.poolId}>
          <List>
            <ListItem>
              <ListItemIcon>
                <MyFootballHelmet />
              </ListItemIcon>
              <ListItemText primary={pool.poolName} />
            </ListItem>
            <ListItem
              className={classNames({
                [classes.activeLink]:
                  location.pathname === '/standings' &&
                  poolId === String(pool.poolId),
              })}
              button
              component={Link}
              onClick={() => setOpen(false)}
              to={{
                pathname: '/standings',
                search: modifyParams(location.search, {
                  poolId: pool.poolId,
                  userId: undefined,
                }),
              }}
            >
              <ListItemIcon>
                <FormatListNumbered
                  className={
                    location.pathname === '/standings' &&
                    poolId === String(pool.poolId)
                      ? classes.activeLink
                      : null
                  }
                />
              </ListItemIcon>
              <ListItemText primary="Standings" />
            </ListItem>
            <ListItem
              className={classNames({
                [classes.activeLink]:
                  location.pathname === '/picks' &&
                  poolId === String(pool.poolId),
              })}
              button
              component={Link}
              onClick={() => setOpen(false)}
              to={{
                pathname: '/picks',
                search: modifyParams(location.search, {
                  poolId: pool.poolId,
                  userId: undefined,
                }),
              }}
            >
              <ListItemIcon>
                <Assignment
                  className={
                    location.pathname === '/picks' &&
                    poolId === String(pool.poolId)
                      ? classes.activeLink
                      : null
                  }
                />
              </ListItemIcon>
              <ListItemText primary="Picks" />
            </ListItem>
          </List>
          <Divider />
        </Fragment>
      ))}
    </Fragment>
  )

  return (
    <React.Fragment>
      <div className={classes.root}>
        <AppBar
          position="absolute"
          className={classNames(classes.appBar, open && classes.appBarShift)}
        >
          <Toolbar disableGutters={!open} className={classes.toolbar}>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={() => setOpen(true)}
              className={classNames(
                classes.menuButton,
                open && classes.menuButtonHidden,
              )}
            >
              <MenuIcon />
            </IconButton>
            <Hidden xsDown implementation="css">
              <MyFootballHelmet className={classes.logo} />
            </Hidden>
            <Typography
              variant="h6"
              color="inherit"
              noWrap
              className={classes.title}
            >
              {poolName || "Pick 'Em"}
            </Typography>
            <Hidden implementation="css" xsDown>
              {self && (
                <Typography>{`Hello ${getDisplayName(self)}`}</Typography>
              )}
            </Hidden>
            <Button
              color="inherit"
              variant="outlined"
              style={{ marginLeft: 10, marginRight: 10 }}
              onClick={logout}
            >
              Logout
            </Button>
          </Toolbar>
        </AppBar>
        <Hidden implementation="css" smUp>
          <Drawer
            variant="temporary"
            classes={{
              paper: classNames(
                classes.drawerPaper,
                !open && classes.drawerPaperClose,
              ),
            }}
            open={open}
            ModalProps={{
              keepMounted: true,
            }}
          >
            {drawerContent}
          </Drawer>
        </Hidden>
        <Hidden implementation="css" xsDown>
          <Drawer
            variant="permanent"
            classes={{
              docked: classes.fullHeight,
              paper: classNames(
                classes.drawerPaper,
                !open && classes.drawerPaperClose,
              ),
            }}
            open={open}
          >
            {drawerContent}
          </Drawer>
        </Hidden>

        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          <Switch>
            <Route
              path="/picks"
              render={() => (
                <Fragment>
                  <Typography variant="h4" gutterBottom>
                    Picks
                    {viewingUser && ` for ${getDisplayName(viewingUser)}`}
                  </Typography>

                  <div className={classes.clearfix}>
                    <div className={classes.selects}>
                      <WeekSelect weekId={weekId} />
                      {grantedUsers.length > 1 && (
                        <UserSelect
                          userId={viewingUser ? viewingUser.id : self.id}
                          users={grantedUsers}
                          location={location}
                        />
                      )}
                    </div>
                  </div>
                  <Week
                    weekId={weekId}
                    poolId={poolId}
                    userId={viewingUser && viewingUser.id}
                  />
                </Fragment>
              )}
            />
            <Route
              path="/standings"
              render={() => (
                <Fragment>
                  <Typography variant="h4" gutterBottom>
                    Standings
                  </Typography>
                  <div className={classes.clearfix}>
                    <div className={classes.selects}>
                      <WeekSelect weekId={weekId} />
                    </div>
                  </div>
                  <Standings weekId={weekId} poolId={poolId} />
                </Fragment>
              )}
            />
            <Redirect
              to={{
                pathname: '/standings',
              }}
            />
          </Switch>
        </main>
      </div>
    </React.Fragment>
  )
}

LoggedInView.propTypes = {
  loadPools: PropTypes.func.isRequired,
  loadUser: PropTypes.func.isRequired,
  addGrantedUsers: PropTypes.func.isRequired,
  poolId: idType,
  history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
  weekId: idType,
  viewingUser: PropTypes.shape({
    username: PropTypes.string.isRequired,
    id: idType.isRequired,
  }),
  self: PropTypes.shape({
    id: idType.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
  }),
  pools: PropTypes.arrayOf(
    PropTypes.shape({
      poolId: idType.isRequired,
      poolName: PropTypes.string.isRequired,
    }),
  ).isRequired,
  invites: PropTypes.arrayOf(
    PropTypes.shape({
      poolId: idType.isRequired,
      poolName: PropTypes.string.isRequired,
    }),
  ).isRequired,
  joinPool: PropTypes.func.isRequired,
  location: PropTypes.shape({
    search: PropTypes.string,
    pathname: PropTypes.string,
  }).isRequired,
  poolName: PropTypes.string,
  grantedUsers: PropTypes.arrayOf(
    PropTypes.shape({
      username: PropTypes.string.isRequired,
      id: idType.isRequired,
    }).isRequired,
  ).isRequired,
}
