import Button from '@material-ui/core/Button'
import Hidden from '@material-ui/core/Hidden'
import Typography from '@material-ui/core/Typography'
import makeStyles from '@material-ui/core/styles/makeStyles'
import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import { getTeamById } from './reducers/selectors'

const styles = (theme) => ({
  btn: {
    margin: `0 ${theme.spacing(1)}px`,
  },
})

const useStyles = makeStyles(styles)

function Team({ className, team, onClick, selected, locked }) {
  const classes = useStyles()

  return (
    <Button
      className={className}
      classes={{ root: classes.btn }}
      disabled={locked}
      onClick={onClick}
      variant={selected ? 'contained' : 'text'}
      color={selected ? 'primary' : 'default'}
    >
      <Hidden implementation="css" smUp>
        <Typography>{team.abbreviation}</Typography>
      </Hidden>
      <Hidden implementation="css" xsDown>
        <Typography>{`${team.city} ${team.teamName}`}</Typography>
      </Hidden>
    </Button>
  )
}
Team.propTypes = {
  className: PropTypes.string,
  team: PropTypes.shape({
    city: PropTypes.string.isRequired,
    teamName: PropTypes.string.isRequired,
    abbreviation: PropTypes.string.isRequired,
  }).isRequired,
  onClick: PropTypes.func,
  selected: PropTypes.bool,
  locked: PropTypes.bool,
}

Team.defaultProps = {
  className: '',
  selected: false,
  locked: false,
  onClick: () => {},
}

const mapState = (state, { teamId }) => {
  const team = getTeamById(state)(teamId)

  return { team }
}

export default connect(mapState)(Team)
