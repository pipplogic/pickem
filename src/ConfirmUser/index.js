import { connect } from 'react-redux'

import ConfirmUser from './ConfirmUser'
import { buildActionCreators } from './confirmDuck'
import { getConfirmState } from '../reducers'

export default connect(
  getConfirmState,
  buildActionCreators(getConfirmState),
)(ConfirmUser)
