import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import Autorenew from '@material-ui/icons/Autorenew'
import Send from '@material-ui/icons/Send'
import PropTypes from 'prop-types'
import qs from 'qs'
import React, { useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import styles from './styles'

const useStyles = makeStyles(styles)

export default function ConfirmUser({
  error,
  handleInputChange,
  handleSubmit,
  history,
  location,
  password,
  setToken,
  showPassword,
  submitting,
  success,
}) {
  useEffect(() => {
    const { token } = qs.parse(location.search, { ignoreQueryPrefix: true })
    if (!token) {
      return history.push('/login')
    }
    setToken(token)
  }, [])

  const classes = useStyles()

  if (success) {
    // TODO display some confirm action
    return <Redirect to="/login" />
  }
  return (
    <form className={classes.root} noValidate onSubmit={handleSubmit}>
      <Typography variant="subtitle1" align="center">
        Set up password
      </Typography>
      <TextField
        label="Password"
        name="password"
        type={showPassword ? 'text' : 'password'}
        value={password}
        error={Boolean(error)}
        onChange={handleInputChange}
      />

      <Button
        type="submit"
        variant="contained"
        color="primary"
        disabled={submitting}
      >
        Set Password
        {submitting && (
          <Autorenew className={`${classes.rightIcon} ${classes.load}`} />
        )}
        {!submitting && <Send className={classes.rightIcon} />}
      </Button>
    </form>
  )
}

ConfirmUser.propTypes = {
  password: PropTypes.string.isRequired,
  error: PropTypes.bool.isRequired,
  handleInputChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
  location: PropTypes.shape({ search: PropTypes.string.isRequired }).isRequired,
  setToken: PropTypes.func.isRequired,
  showPassword: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  success: PropTypes.bool.isRequired,
}
