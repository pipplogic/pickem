import Button from '@material-ui/core/Button'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import ArrowDropDown from '@material-ui/icons/ArrowDropDown'
import React, { useState } from 'react'
import PropTypes from 'prop-types'

export default function Dropdown({ value, options, menuItemProps }) {
  const [anchorEl, setAnchorEl] = useState(null)
  const [id] = useState(`dropdown-${Math.random()}`)

  function handleOpen(ev) {
    setAnchorEl(ev.currentTarget)
  }
  function handleClose() {
    setAnchorEl(null)
  }

  return (
    <React.Fragment>
      <Button aria-controls={id} aria-haspopup="true" onClick={handleOpen}>
        {options
          .filter((option) => String(option.value) === String(value))
          .map((option) => option.label)}
        <ArrowDropDown />
      </Button>
      <Menu
        id={id}
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {options.map((option) => (
          <MenuItem
            key={option.value}
            {...menuItemProps(option)}
            onClick={() => handleClose(option)}
          >
            {option.label}
          </MenuItem>
        ))}
      </Menu>
    </React.Fragment>
  )
}

Dropdown.propTypes = {
  className: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  options: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        .isRequired,
      label: PropTypes.string.isRequired,
    }),
  ).isRequired,
  menuItemProps: PropTypes.func,
}

Dropdown.defaultProps = {
  value: null,
  menuItemProps: () => {},
}
