import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import PropTypes from 'prop-types'

import { getLoginState } from './reducers'
import { isInitialized, isLoggedIn } from './Login/loginDuck'

const mapState = (state) => ({
  initialized: isInitialized(getLoginState(state)),
  loggedIn: isLoggedIn(getLoginState(state)),
})

const RequireLogin = ({
  inverted,
  initialized,
  loggedIn,
  children,
  redirect,
  redirectTo,
}) => {
  if (!initialized) {
    return null
  }
  const shouldShow = (loggedIn && !inverted) || (!loggedIn && inverted)

  if (shouldShow) {
    return children
  }
  return redirect || redirectTo ? (
    <Redirect to={redirectTo || '/login'} />
  ) : null
}

RequireLogin.propTypes = {
  inverted: PropTypes.bool,
  initialized: PropTypes.bool,
  loggedIn: PropTypes.bool,
  children: PropTypes.node,
  redirect: PropTypes.bool,
  redirectTo: PropTypes.string,
}

RequireLogin.defaultProps = {
  inverted: false,
  initialized: false,
  loggedIn: false,
  children: null,
  redirect: false,
  redirectTo: null,
}

export default connect(mapState)(RequireLogin)
